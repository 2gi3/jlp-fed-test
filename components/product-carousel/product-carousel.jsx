import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ image , imageAlt}) => {
  return (
    <div className={styles.productCarousel} aria-label="images carousel">
      <img src={image} alt={imageAlt} />
    </div>
  );
};

export default ProductCarousel;
