import itemsArray from "../mockData/data.json"

const loadProductAttributes = (item,attributes) => {
    {
      for (const [key, value] of Object.entries(item.dynamicAttributes)) {
        { attributes.push(`<div style="display:flex; flex-wrap:wrap; justify-content:space-between; padding:10px 0; color: #707070; border-bottom: 1px solid #dbdbdb " ><div>${key}:</div><div style="text-align: right; color: #707070;">${value}</div></div>`) }
      }
    };
  }

export default loadProductAttributes