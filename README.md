Unresolved problems:
-I was not able to fetch data from the URL provided so for now the project can only use the mockData 
-I don't understand how or where the SCSS files are compiled in this project, so to style the [id].jsx 
  i compiled my SCSS into a CSS file in the same folder which i imported into [id].jsx, as a result of using my compiler
  some untracked css files may have been created with the corresponding existing SCSS files and need to be deleted.
-I can't find the main HTML file to add the language attribute.
-The carousel is just a single picture

Resolved problems(after the first push):
-bug-1: Sometimes the Product specification in [id].jsx does't load the first time the browser opens, if this happens, the browser must be frfreshed for it to appear.
-bug2: When the viewport reaches a width of 1194px the carousel starts widening again misaligning the right border
-bug3 In the [id].jsx file, i was not able to reference the ID from the URL as i wasn't able to make the windows object work


Notes:
-The Node.js version i used for this project is 14.18.0
-Although i have started learning about TDD and Jest thanks to this project, in order to deliver within reasonable time
  i have written the code in the way i am used to, and i plan to  keep learning about it and redo this project following
  this much much more efficient and new-to-me developement approach.
-If possible, i would like to ask for a list of things i should learn in order to complete this project, and any feedback at all will be 
  much appreciated whether or not i have a chance to reach the next step in this job application. (feel free to email me at gippolito@hotmail.co.uk )



# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. 

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.
