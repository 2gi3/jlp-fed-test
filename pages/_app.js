import Layout from "../components/layout/layout";
import ProductCarousel from "../components/product-carousel/product-carousel"
import ProductListItem from "../components/product-list-item/product-list-item"
import "../styles/globals.scss";
import items from "../mockData/data.json"

function MyApp({ Component, pageProps , }) {
  return (
    <Layout type="netherworld">
{/* added data prop */}
      <Component data={items} {...pageProps} />
      {/* <ProductCarousel image={items} /> */}
      {/* <ProductListItem image={} price={} description={} /> */}
    </Layout>
  );
}

export default MyApp;
