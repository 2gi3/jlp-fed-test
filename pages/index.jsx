import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import React, { useEffect, useState } from 'react';
import ProductListItem from "../components/product-list-item/product-list-item";

// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }
// console.log(getServerSideProps() )
// const ftc =()=>{
//   fetch('https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI',
//   { mode: 'no-cors'})
//   .then((response) => response.json())
//   .then((json) => console.log(json));
// }

// ftc()

const Home = ({ data }) => {
 
  let items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="dishwashers, john luis" content="shopping" />
        <meta name="description" content="Dishwashers available at John Lewis"/>
      </Head>
      <div>
        <div className={styles.header}>
        <h1>Dishwashers({items.length})</h1>
        </div>
        <div className={styles.content}>
          {items.map((item, index) => (
// added conditional operator to display only the first 20 objects in the array
            index< 20? <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <div className={styles.link}>
                <div className={styles.contentProduct}>
                  <div className={styles.contentImg}>
                    <img src={item.image} alt="" style={{ width: "100%" }} />
                  </div>
                  <div className={styles.contentTitle}>{item.title}</div>
                  <div className={styles.contentPrice}>&#163;{item.price.now}</div>
                </div>
              </div>
            </Link>: false
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
