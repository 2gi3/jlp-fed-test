import ProductCarousel from "../../components/product-carousel/product-carousel";
import itemsArray from "../../mockData/data.json"
import React, { useEffect, useState } from 'react';
import Link from "next/link";
import styles from "./productPage.module.css"
import Head from "next/head";
import loadProductAttributes from "../../functions/functions";




// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();
//   console.log(data)

//   return {
//     props: { data },
//   };
// }



const ProductDetail = ({ data }) => {

  // find the product to display by it'snp index in the array 
  const url = window.location.pathname;
  let last = url.substring(url.lastIndexOf("/") + 1, url.length);

  const items = itemsArray.products
  let index = items.findIndex(entry => entry.productId === last);
  console.log(index);

  const item = items[index]
  const attributes = []



  // iterates through the "dynamicAttributes" object inside the variable "item" and 
  // pushes them into the array "attributes"

  loadProductAttributes(item, attributes)

  // iterates through the array "attributes" to produce a <div> containing all of the elements listed in the .productSpecificationsList
  const attributesList = attributes.map((data, index) => {
    return (
      <>
        {index < 7 ? <div dangerouslySetInnerHTML={{ __html: data }} /> : false}
      </>
    )
  })

  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="dishwashers, john luis" content="shopping" />
        <meta name="description" content="Dishwashers available at John Lewis" />
      </Head>
      <div className={styles.productHeader}>
        <div className={styles.navButton}><Link href={{ pathname: "/" }}> &#60; </Link></div>
        <div className={styles.productHeaderTitle} dangerouslySetInnerHTML={{ __html: item.title }} />
      </div>

      <div>
        <h1></h1>
      </div>
      <div className={styles.outerContainer}>
        <div>
          <div className={styles.hero}>
            <div className={styles.carousel}>
              {/* <ProductCarousel image={data.media.images.urls[0]} /> */}
              <ProductCarousel image={item.image} imageAlt={item.title} />
            </div>
            <div className={styles.priceBox}>
              <div className={styles.priceBoxPrice}>
                <h1>&#163;{item.price.now}</h1>
              </div>
              <div className={styles.priceBoxAdditionalServices}>
                {/* hard coded */}
                <p>Claim an extra 3&#160;years guarantee via redemption</p>
                {/* <div>{item.additionalServices.includedServices}</div> */}
              </div>
              <div className={styles.priceBoxSpecialOffer}>
                <div>
                  {/* hard coded */}
                  <p>Special Offer: &#163;50 trade in untill the 15 Dec</p>
                  <p>{item.displaySpecialOffer}</p>
                </div>
              </div>
              <div className={styles.priceBoxIncludedExtras}>
                <p>2 year guarantee included</p>
              </div>
            </div>
          </div>
          <div className={styles.productInformations}>
            <div className={styles.productInformationsHeader}>
              <p>Product information</p>
            </div>
            <div className={styles.productInformationsBody}>
              <div className={styles.productInformationsId}>
                <p>Product code: {item.productId}</p>
              </div>
              {/* hard coded */}
              <div className={styles.productInformationsDescription}>
                <p>The {item.title} features a range of options for versatile, fast and hygenic cleaning.
                  A child lock for extra safety and 14 place settings make this machine ideal for family households.
                  The A++ energy ratingwill help you save money on your energy bills
                  while reducing your impact on the environment</p>
              </div>
              <div className={styles.productInformationsReadMore}>
                <p>Read more</p>
              </div>
            </div>
          </div>
          <div className={styles.productSpecifications}>
            <div className={styles.productSpecificationsHeader}>
              <p>Product specification</p>
            </div>
            <div className={styles.productSpecificationsList}>
              {/* {data.details.features[0].attributes.map((item) => (
            <li>
              <div><div dangerouslySetInnerHTML={{ __html: item }} /></div>
            </li>
              ))} */}
              {attributesList}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;


